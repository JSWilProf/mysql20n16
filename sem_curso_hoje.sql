-- Os professores que não estão ministrando cursos hoje
-- * nome do professor
-- * e-mail do professor
-- ordenados por nome em ordem crescente
select nome, email
from professores
where id not in (
	select professor_id 
	from turmas
	where dt_inicio <= now()
	  and dt_fim >= now()
)
order by nome;


