-- O curso da turma mais antiga.
select c.nome, t.dt_inicio
from turmas t, cursos c
where t.curso_id = c.id
order by t.dt_inicio
limit 1;

select nome
from cursos
where id = (
	select curso_id
	from turmas
	order by dt_inicio
	limit 1
);