-- Os cursos que estão em andamento
-- * nome do curso
-- * data de inicio
-- * data de finalização
-- ordenados por nome e data de início.
select c.nome, t.dt_inicio, t.dt_fim
from turmas t
	join cursos c
    on t.curso_id = c.id
where t.dt_inicio <= now()
  and t.dt_fim >= now()
order by c.nome, t.dt_inicio;