create view cursos_ccna as
select t.id as curso_id, c.nome as curso , p.nome as prof, t.dt_inicio, t.dt_fim 
from turmas as t, cursos as c, professores as p
where curso_id = 2
  and t.curso_id = c.id
  and t.professor_id = p.id;
