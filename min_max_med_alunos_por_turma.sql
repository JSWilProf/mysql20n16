-- A máxima, mínima e média de alunos por turma.
select min(qtd_alunos), max(qtd_alunos), round(avg(qtd_alunos),0)
from (
	select count(*) qtd_alunos 
	from turmas_has_alunos
	group by turma_id
) qtd_alunos_por_turma;
-- group by qtd_alunos;


select count(*) qtd_alunos 
	from turmas_has_alunos
	group by turma_id
	order by qtd_alunos;