-- MySQL Workbench Synchronization
-- Generated: 2020-03-05 20:02
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: penawjs

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER TABLE `mysql2016-0`.`cursos_dos_alunos` 
ADD INDEX `fk_cursos_dos_alunos_aluno1_idx` (`n_matricula` ASC) VISIBLE;
;

ALTER TABLE `mysql2016-0`.`sala` 
ADD INDEX `fk_sala_professor_idx` (`id_prof` ASC) VISIBLE;
;

ALTER TABLE `mysql2016-0`.`cursos_dos_alunos` 
ADD CONSTRAINT `fk_cursos_dos_alunos_sala1`
  FOREIGN KEY (`n_sala`)
  REFERENCES `mysql2016-0`.`sala` (`n_sala`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_cursos_dos_alunos_aluno1`
  FOREIGN KEY (`n_matricula`)
  REFERENCES `mysql2016-0`.`aluno` (`n_matricula`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `mysql2016-0`.`sala` 
ADD CONSTRAINT `fk_sala_professor`
  FOREIGN KEY (`id_prof`)
  REFERENCES `mysql2016-0`.`professor` (`id_prof`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
