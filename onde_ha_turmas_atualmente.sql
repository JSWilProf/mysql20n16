-- Os cursos onde há turmas atualmente.
select c.id, c.nome, t.dt_inicio, t.dt_fim
from turmas t, cursos c
where t.curso_id = c.id
  and dt_inicio <= now()
  and dt_fim >= now();

select id, nome
from cursos
where id in (
	select curso_id
	from turmas
	where dt_inicio <= now()
	  and dt_fim >= now()
);