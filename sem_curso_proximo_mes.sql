-- Os professores que estarão sem turma no próximo mês
-- * nome do professor
-- * email do professor
-- ordenado por nome.
select nome, email
from professores
where id not in (
	select professor_id 
	from turmas
	where dt_fim >= last_day(last_day(now()) + interval 1 day)
      and dt_inicio <= last_day(now()) + interval 1 day
)
order by nome;

	 