INSERT INTO curso
	(idCurso, nome)
VALUES 
	(1, 'Desenvolvimento Back-end cem Java com Spring Boot'),
    (2, 'Desenvilvimento Mobile com Flutter'),
    (3, 'Desenvolvimento Web com React e Redux'),
    (4, 'Banco de Dados Mysql - Fundamentos');

INSERT INTO professor
	(idProfessor, nome, documento, email)
VALUES 
	(1, 'Wilson', '2345.6', 'wilson@sp.senai.br'),
    (2, 'Eduardo', '32423.8', 'eduardo@sp.senai.br');

INSERT INTO turma
	(idTurma, dt_inicio, dt_fim, duracao, idCurso, idProfessor)
VALUES 
	(1, '2020-01-08','2020-04-19','03:00:00', 1, 1),
    (2, '2020-01-18','2020-02-20','03:00:00', 2, 1),
    (3, '2020-02-04','2020-05-02','03:00:00', 3, 2),
    (4, '2020-03-08','2020-05-20','03:00:00', 4, 1);

INSERT INTO aluno
	(idAluno, nome, documento, email)
VALUES 
	(1, 'Cristiano', '39283', 'cristiano@gmail.com'),
    (2, 'Guilherme', '93844', 'guilherme@hotmail.com'),
    (3, 'Luiz', '83732', 'luiz@yahoo.com'),
    (4, 'Ricardo', '74623', 'ricardo@gmail.com'),
    (5, 'Tiago', '10293', 'tiago@hotmail.com');

INSERT INTO aluno_turma
	(idAluno, idTurma)
VALUES 
	(1, 1), (2, 1), (1, 2), (3, 2), (4, 3);
