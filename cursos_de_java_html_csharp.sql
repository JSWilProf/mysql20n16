-- Os cursos de HTML, JAVA e C# Ordenados por Nome Decrescente.
select nome
from cursos
where nome like '%html%'
   or nome like '%java%'
   or nome like '%c#%'
order by nome desc;