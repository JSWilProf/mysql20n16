CREATE TABLE aluno (
  idAluno int NOT NULL AUTO_INCREMENT,
  nome varchar(100) NOT NULL,
  documento varchar(100) NOT NULL,
  email varchar(100) NOT NULL,
  PRIMARY KEY (idAluno)
);

CREATE TABLE curso (
  idCurso int NOT NULL AUTO_INCREMENT,
  nome varchar(100) NOT NULL,
  PRIMARY KEY (idCurso)
);

CREATE TABLE professor (
  idProfessor int NOT NULL AUTO_INCREMENT,
  nome varchar(100) NOT NULL,
  documento varchar(100) NOT NULL,
  email varchar(100) NOT NULL,
  PRIMARY KEY (idProfessor)
);

CREATE TABLE turma (
  idTurma int NOT NULL AUTO_INCREMENT,
  dt_inicio date NOT NULL,
  dt_fim date NOT NULL,
  duracao time NOT NULL,
  idCurso int NOT NULL,
  idProfessor int NOT NULL,
  PRIMARY KEY (idTurma),
  
  foreign key (idCurso) references curso (idCurso),
  foreign key (idProfessor) references professor (idProfessor)
);

CREATE TABLE aluno_turma (
	idAluno int NOT NULL,
	idTurma int NOT NULL,
	PRIMARY KEY (idAluno, idTurma),
	
	foreign key (idAluno) references aluno (idAluno),
	foreign key (idTurma) references turma (idTurma)
);