-- As turmas do curso de “MySql - Fundamentos”
-- * nome do curso
-- * data de início
-- * data de finalização
-- * nome do professor
-- ordenadas por data de início em ordem crescente
select c.nome, t.dt_inicio, t.dt_fim, p.nome
from turmas t
	join cursos c
    on t.curso_id = c.id
    
    join professores p
    on t.professor_id = p.id
where c.nome like '%mysql - fundamentos%'
order by t.dt_inicio;