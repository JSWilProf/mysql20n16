CREATE TABLE `carro` (
  `idCarro` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `marca` varchar(50) NOT NULL,
  `dataLancamento` date NOT NULL,
  `ano` int(11) NOT NULL,
  `aceleracao` time NOT NULL,
  PRIMARY KEY (`idCarro`)
);

alter table carro
drop column aceleracao,
add column potencia int unsigned not null;

alter table carro
modify column potencia int not null;